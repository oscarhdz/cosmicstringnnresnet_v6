"""
by Razvan Ciuca, 2017

This script takes the ascii maps produced by CMBEDGE in the ascii_dir and converts them to numpy maps and saves them,
you need to specify the ascii_dir where you placed all the cmbedge ascii files.

This script takes a relatively long time since reading from ascii files is so slow
"""


import numpy as np
from config import *


# loads a cmbedge file and returns a numpy array
def numpy_from_cmbedge_file(filename, size=512):
    map = np.ndarray([size, size], dtype=np.float32)
    with open(filename) as file:
        for line in file:
            if line != '':
                l = line.split('\t')
                map[int(l[0]), int(l[1])] = float(l[2])

    return map


# function to load n_maps maps from ascii_dir, returns
def get_maps(ascii_dir, n_maps):

    # arrays which will contain maps of their respective types
    numpy_s_maps = []
    numpy_a_maps = []
    numpy_g_maps = []

    # iterate over the number of maps we desire to load
    for n in range(0, n_maps):
        filename = ascii_dir + "stringMap_" + str(n) + '.dat'
        ans_filename = ascii_dir + "strings_only_Map_" + str(n) + '.dat'
        g_filename = ascii_dir + "gauss_Map_" + str(n) + '.dat'

        data = numpy_from_cmbedge_file(filename).reshape([1, 1, 512, 512])
        ans_data = numpy_from_cmbedge_file(ans_filename).reshape([1, 1, 512, 512])
        g_data = numpy_from_cmbedge_file(g_filename).reshape([1, 1, 512, 512])

        numpy_s_maps.append(data)
        numpy_a_maps.append(ans_data)
        numpy_g_maps.append(g_data)

        print("done map " + str(n))

    numpy_s_maps = np.concatenate(numpy_s_maps, 0)
    numpy_a_maps = np.concatenate(numpy_a_maps, 0)
    numpy_g_maps = np.concatenate(numpy_g_maps, 0)

    return numpy_s_maps, numpy_a_maps, numpy_g_maps

# if you are calling this script directly, you want to compute the frequency_computation maps
if __name__ == '__main__':
    numpy_s_maps, numpy_a_maps, numpy_g_maps = get_maps(frequency_computation_maps_ascii_dir, n_cmbedge_ascii_maps_to_load_for_frequency_calculation)

    np.save("../data/cmbedge_s_maps_frequency_computation.npy", numpy_s_maps.astype(np.float32))
    np.save("../data/cmbedge_a_maps_frequency_computation.npy", numpy_a_maps.astype(np.float32))
    np.save("../data/cmbedge_g_maps_frequency_computation.npy", numpy_g_maps.astype(np.float32))

# otherwise, this script is being called from feeder, in which case we're computing training maps
else:
    numpy_s_maps, numpy_a_maps, numpy_g_maps = get_maps(training_maps_ascii_dir, n_cmbedge_ascii_maps_to_load_for_training)

    np.save("../data/cmbedge_s_maps.npy", numpy_s_maps.astype(np.float32))
    np.save("../data/cmbedge_a_maps.npy", numpy_a_maps.astype(np.float32))
    np.save("../data/cmbedge_g_maps.npy", numpy_g_maps.astype(np.float32))
