"""
by Razvan Ciuca, 2017. OscarH: I labelled my changes, additions with #OH

This file contains the routines to compute posteriors for given sky maps, all the relevent computation parameters are
again located in config.py

(SOLVED) possible reasons for misplaced peaks:

1. too little data, can solve by using translated maps/generating more maps 
   VERDICT: not the issue, the log sum corresponds to a small part of the posterior
2. borders are not removed 
   VERDICT: contributes to it, removing borders helps, but not at low Gmu
3. breakdown of independence assumptions at low G\mu (need a way to test validity of assumptions)
   VERDICT: suspected cause of the fact that the posteriors are much sharper than the observed accuracy in Gmu
4. Errors in the gaussian calculations (very possible, though it shouldn't make that much of a difference)
   VERDICT: unlikely to be the cause at low Gmu, doesn't affect much
5. number of bins is too small (frequencies with 200 bins are being computed right now)
   VERDICT: TRUE, increasing bins to 400 instead of 30 yields drastic accuracy improvements
            try going to the limit by using 1000 bins instead, use the cedar cluster for that
6. difference in normalization between the frequency calculation vs. candidate maps creation
   VERDICT: no, I'm doing the exact same thing in both create_candidate_sky_map and in compute_frequencies_bayesian
7. better interpolation between maps at different Gmu? unlikely
8. using the 2 dicrete variables instead of the 1
   VERDICT: instead of overestimating, it now underestimates slightly, low Gmu bump still bad
9. Minimizing the denominator instead of maximizing:
   didn't help at low Gmu
   


"""

import torch as t
from torch.autograd import Variable
import torch.nn.functional as F
import torch.nn as nn
import numpy as np
import numpy.fft as fft
import pickle
from scipy.misc import imshow
from scipy.misc import factorial2
import os
import sys
import time

#oh#from feeder import DataFeeder
from model_def import *
#oh#from utils import power_spectrum, numpy_from_cmbedge_file, write_image_cmbedge_format, set_border_to_minimum, normalize_map, get_top_k_pixels
from config import *


# Below are functions which output multiple possible priors for Gmu

def uniform_prior(Gmu):
    if Gmu<1e-7:
        return 1e7 # need to normalize to an integral of 1
    else:
        return 0


def exponential_prior(Gmu, decay = 3.32e-8):
    return np.exp(-Gmu/decay)/decay


# from https://en.wikipedia.org/wiki/Error_function
# log erfc for x>>0
def log_erfc(x, n=10):

    y = -0.5*np.log(np.pi) - x**2
    y -= np.log(x)

    if x<10:
        y += np.log(sum([(-1)**i * factorial2(2*i-1)/(2*x**2)**i for i in range(0, n)]))

    return y


# translates numpy maps assuming periodic boundary conditions
def translate_maps(maps, t_x, t_y):
    if t_x == 0 and t_y == 0:
        return maps
    elif t_x == 0:
        slice_1 = np.concatenate([maps[:, :, t_y:], maps[:, :, :t_y]], 2)
        return slice_1
    elif t_y == 0:
        slice_2 = np.concatenate([maps[:, t_x:, :], maps[:, :t_x, :]], 1)
        return slice_2
    else:
        slice_1 = np.concatenate([maps[:, t_x:, t_y:], maps[:, t_x:, :t_y]], 2)
        slice_2 = np.concatenate([maps[:, :t_x, t_y:], maps[:, :t_x, :t_y]], 2)
        return np.concatenate([slice_1, slice_2], 1)


# this returns a numpy map corresponding to P(xi | sky_map, Gmu), it basically interpolates between the Gmus
# used in the frequency calculation
def get_conditioned_prediction_map(pred_maps, conditional_Gmu):

    sorted_Gmu = sorted(pred_maps.keys())[::-1]

    if conditional_Gmu > sorted_Gmu[0]:
        print('conditional Gmu too large, it needs to be smaller than ' + str(sorted_Gmu[0]))
    elif conditional_Gmu < sorted_Gmu[-1]:
        print('conditional Gmu too small, it needs to be larger than ' + str(sorted_Gmu[-1]))

    for i in range(0, len(sorted_Gmu)-1):
        if conditional_Gmu < sorted_Gmu[i] and conditional_Gmu > sorted_Gmu[i+1]:

            p = (conditional_Gmu - sorted_Gmu[i])/(sorted_Gmu[i+1]-sorted_Gmu[i])
            return (pred_maps[sorted_Gmu[i]]*(1-p) + pred_maps[sorted_Gmu[i+1]]*p).squeeze()


# does the same thing as rescale_map_old, but 5 times faster, with much less intuitive code
def rescale_map(prediction_map, intervals, freq, eps=1e-7):

    # first, sort the prediction_map and produce the indices of the sorted values
    sorted_indices = np.argsort(prediction_map.reshape(-1))
    # produce the indices needed to unsort the map, i.e. prediction_map.reshape(-1)[sorted_indices] is the array of
    # sorted values, whereas prediction_map.reshape(-1)[sorted_indices][inverse_indices] is equal to
    # prediction_map.reshape(-1)
    inverse_indices = np.argsort(sorted_indices)

    # this weird function finds, for every value in intervals, the number of values in prediction_maps smaller than
    # it, i.e. positions_of_interval_locations[i] = number of pixels with prediction values smaller than intervals[i]
    positions_of_interval_locations = np.searchsorted(prediction_map.reshape(-1), intervals, sorter=sorted_indices)

    # create an array of values from freq[i], each one repeated positions_of_interval_locations[i + 1] -
    # positions_of_interval_locations[i] times
    output = np.concatenate([[freq[i]] * (positions_of_interval_locations[i + 1] -
                                                         positions_of_interval_locations[i]) for i in
                                            range(0, len(intervals) - 1)])

    # append epsilons to the beginning and to the end
    output = np.concatenate([[eps] * positions_of_interval_locations[0], output,
                             [eps] * (sorted_indices.shape[0] - positions_of_interval_locations[-1])])

    # unsort the output in the same manner as we sorted the prediction_map values
    output = output[inverse_indices].reshape(prediction_map.shape)
    return output


# old way of rescaling maps, no longer used anywhere, but it is here for conceptual clarity, it does the exact same
# thing as rescale_map, but takes about 5 times longer for size 512 maps because of the redundant indexing
def rescale_map_old(prediction_map, intervals, freq, eps=1e-7):
    output = np.zeros(prediction_map.shape)

    # set all pixels outside this range to a small number
    output[prediction_map < intervals[0]] = eps
    output[prediction_map >= intervals[-1]] = eps
    # iterate over the interval locations and rescale the output for the i-th bin to the i-th freq value
    for i in range(0, len(intervals) - 1):
        output[(prediction_map >= intervals[i]) * (prediction_map < intervals[i + 1])] = freq[i]

    return output


# computes P(f_ij | x_ij=0, G\mu), P(f_ij | x_ij=1, G\mu) and P(x_ij=1 | f_ij, G\mu) for all pixels i,j
def probability_maps(sky_map, model, border=border):

    # notice that we are loading frequencies from the model itself, no need to manually specify the file
    frequencies = model.frequencies[-1]
    # unpack the frequencies array
    P_string_given_f_Gmu, P_f_given_Gmu_string = frequencies

    # evaluate the model on the sky_map
    inputs = Variable(sky_map)
    inputs.volatile = True
    prediction = model.forward(inputs).data.squeeze().cpu().numpy()
    prediction = prediction[border:-border, border:-border]

    # initalize the dictionaries which will contain the output
    P_xi_given_f_Gmu = {}
    P_f_given_xi_Gmu = {}
    eps = 1e-7  # small number,

    # doing P_xi_given_f_Gmu
    for Gmu in P_string_given_f_Gmu.keys():
        intervals, freq = P_string_given_f_Gmu[Gmu]
        P_xi_given_f_Gmu[Gmu] = rescale_map(prediction, intervals, freq, eps=eps)

    # doing P_f_given_xi_Gmu
    for Gmu in P_f_given_Gmu_string.keys():
        stats_on, stats_off = P_f_given_Gmu_string[Gmu]
        hist_on_string, intervals_on_string = stats_on
        hist_off_string, intervals_off_string = stats_off

        # the first index represents the probability conditioned on string=True, the second on string=False
        P_f_given_xi_Gmu[Gmu] = np.zeros([2, prediction.shape[0], prediction.shape[1]])

        P_f_given_xi_Gmu[Gmu][0] = rescale_map(prediction, intervals_on_string, hist_on_string, eps=eps)
        P_f_given_xi_Gmu[Gmu][1] = rescale_map(prediction, intervals_off_string, hist_off_string, eps=eps)

    return prediction, P_xi_given_f_Gmu, P_f_given_xi_Gmu


# function which computes the expectation and standard deviation of the number of pixel differences between a_maps[i]
# and the maximal_map
def get_distribution_of_pixel_overlap(maximal_map, a_maps):

    # make sure they're boolean maps
    maximal_map = maximal_map > 0.5
    a_maps = a_maps > 0.5

    # the following line augments the dataset with many translated maps, used as an example, not necessary
    # overlaps = np.array([[(maximal_map == x).sum() for x in translate_maps(a_maps, i, 0)] for i in range(0, 100, 10)])
    overlaps = np.array([(maximal_map == x).sum() for x in a_maps])

    return overlaps.mean(), overlaps.std()


# this function returns the map \xi which maximizes log(P(f|xi, G\mu)/P(xi | f, G\mu))
def get_maximal_map(P_xi_given_f_Gmu, P_f_given_xi_Gmu, Gmu):

    P_xi_given_f = get_conditioned_prediction_map(P_xi_given_f_Gmu, Gmu)
    P_f_given_xi = get_conditioned_prediction_map(P_f_given_xi_Gmu, Gmu)

    log_prob_on = np.log(P_f_given_xi[0]) - np.log(P_xi_given_f)
    log_prob_off = np.log(P_f_given_xi[1]) - np.log(1-P_xi_given_f)

    maximal_map = log_prob_on > log_prob_off
    log_prob_maximal_map = (log_prob_on*maximal_map + log_prob_off*(1-maximal_map))

    # mean change in log probability after changing 1 pixel from the map
    # it's really this expression:
    # (log_prob_on * maximal_map + log_prob_off * (1 - maximal_map)) -
    # (log_prob_on * (1-maximal_map) + log_prob_off * maximal_map)

    expected_1_pixel_change = (log_prob_on * (1 - 2*maximal_map) + log_prob_off * (2*maximal_map - 1)).mean()

    return maximal_map, log_prob_maximal_map.sum(), expected_1_pixel_change


# function which brings everything back together and computes the posterior, see the paper for detailed explanation
def bayesian_posterior(prior, sky_map, model, a_maps, border=border, write_to=None):

    posterior = {}

    # compute and save rescaled prediction maps
    a_maps = a_maps[:, border:-border, border:-border]
    print('starting to compute required probability maps')
    prediction, P_xi_given_f_Gmu, P_f_given_xi_Gmu = probability_maps(sky_map, model, border=border)
    print('finished computing required probability maps. Now saving...')

	#OH: added the saving of prediction maps
    write_raw_pred_to      = write_to+'raw_prediction_maps'      #prediction
    write_rescaled_pred_to = write_to+'rescaled_prediction_maps' #P_xi_given_f_Gmusave_raw_prediction_maps_to = posteriors_directory+'raw_prediction_maps'           #prediction
    # in v4  pred_maps = prediction_maps(sky_map, model, frequencies, prior)
    # this was and still is a dictionary for which pred_maps[Gmu] contains the map of values P(\xi | \delta_sky, G\mu)
    # the keys of the dictionary are the keys of the frequencies dictionary
    # Now we use probability_maps which returns the unscaled prediction, P_xi_given_f_Gmu, P_f_given_xi_Gmu
    # the second of these is the rescaled map that correspond to our old pred_maps, i.e.: 
    # raw_pred_maps, rescaled_pred_maps, P_f_given_xi_Gmu = probability_maps(sky_map, model, border=border)   
    pickle.dump(prediction, open(write_raw_pred_to, 'wb'))
    pickle.dump(P_xi_given_f_Gmu, open(write_rescaled_pred_to, 'wb'))
    print('...finished saving.')
    
    # for timing purposes
    last_posterior = 0
    n_Gmu_done = 0

    # define the plotting limits as being slighlty inside the limits of the frequency calculation
    smallest_Gmu = np.min(list(P_xi_given_f_Gmu.keys()))*1.01
    largest_Gmu = np.max(list(P_xi_given_f_Gmu.keys()))*0.99

    if overwrite_Gmu_limits:
        smallest_Gmu = forced_smallest_Gmu
        largest_Gmu = forced_largest_Gmu

    Gmu_list = [largest_Gmu * (smallest_Gmu/largest_Gmu)**(i/N_Gmu_to_plot) for i in range(0, N_Gmu_to_plot)]

    for Gmu in Gmu_list:
        print('starting Gmu ' + str(Gmu))
        start_time = time.clock()

        maximal_map, log_prob, expected_change = get_maximal_map(P_xi_given_f_Gmu, P_f_given_xi_Gmu, Gmu)
        mean_overlap, std_overlap = get_distribution_of_pixel_overlap(maximal_map, a_maps)
        erfc_argument = - (mean_overlap + std_overlap**2 * expected_change)/(np.sqrt(2)*std_overlap)

        posterior[Gmu] = 0
        posterior[Gmu] += np.log(prior(Gmu))
        posterior[Gmu] += log_prob  # (log_prob+log_prob_2)/2   # heuristic
        posterior[Gmu] += mean_overlap*expected_change + (std_overlap**2 * expected_change**2)/2
        posterior[Gmu] += np.log(0.5)
        posterior[Gmu] += log_erfc(erfc_argument)
        print(mean_overlap*expected_change + (std_overlap**2 * expected_change**2)/2 + log_erfc(erfc_argument))

        end_time = time.clock()
        n_Gmu_done += 1
        print("posterior = " + str(posterior[Gmu]) + ', delta from last = ' + str(posterior[Gmu] - last_posterior) +
              ', time taken = ' + str(end_time - start_time) + ' sec, time remaining = ' +
              str((end_time - start_time) * (N_Gmu_to_plot - n_Gmu_done) / 60) + ' min')
        last_posterior = posterior[Gmu]

    # we normalize the posteriors to have the maximum be at 0.0
    max_posterior = np.max(list(posterior.values()))

    write_posterior_to = write_to+'_posterior'
    if write_to is not None:
        with open(write_posterior_to, 'w') as file:
            for Gmu in Gmu_list:
                file.write(str(Gmu) + ' ' + str((posterior[Gmu]-max_posterior)) + '\n')

    return posterior




# main loop, for evaluating a list of maps at a time
def bayesian_main():
    # create the posteriors folder if it doesn't already exist
    os.system('mkdir ' + posteriors_directory)
    model = load_model(compute_posteriors_with_this_model_filename)
    if t.cuda.is_available():
        model = model.cuda()

    for sky_map_filename in candidate_maps_filename_list:

        sky_map = np.load(sky_map_filename).reshape([1, 1, 512, 512])
        sky_map = t.from_numpy(sky_map).cuda() if t.cuda.is_available() else t.from_numpy(sky_map)
        sky_map /= sky_map.std()

        a_maps = np.load(a_maps_filename_used_for_bayesian) > 0.5
        a_maps = a_maps[number_of_a_maps_not_used:].squeeze()

        #OH
        write_to_prefix = posteriors_directory + sky_map_filename.split('/')[-1].split('.npy')[0] #+ '_posterior'

        # finally compute the posterior and write it to the given file
        posterior = bayesian_posterior(exponential_prior, sky_map, model, a_maps, border=border, write_to=write_to_prefix)

if __name__ == '__main__':
    bayesian_main()