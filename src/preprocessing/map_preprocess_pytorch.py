"""
By Razvan Ciuca, 2017

This file contains code to convolve the 1024 by 1024 maps contained in ./numpy_maps by a 3 by 3 gaussian filter,
smoothing them and bringing them down to a 512 by 512 size
"""

# import stuff
import os
import numpy as np
import torch as t
import torch.nn as nn
from torch.autograd import Variable
from config import *


# x is a 3-dimensional torch tensor and pad_size is an integer > 0
# this pads the image by pad_size on all sides using repeated pixels from the opposite side
# this is necessary for the convolution operation to produce a map of the same size
def pad_repeating(x, pad_size):
    s2 = x.size(2)
    s3 = x.size(3)
    x1 = t.cat([x[:, :, s2 - pad_size: s2], x, x[:, :, 0:pad_size]], 2)
    x2 = t.cat([x1[:, :, :, s3 - pad_size: s3], x1, x1[:, :, :, 0:pad_size]], 3)
    return x2


# define the convolution kernel to be applied to the maps, right now this is a simple 3 by 3 matrix with a exp(-(x^2)/2)
# dependence, normalized to have its elements summed to 1
# this function also downscales the convolved images by a factor of 2 in each dimension
def convolve(maps):

    # defining the convolution kernel W
    W = np.ndarray([3, 3])
    for i in range(3):
        for j in range(3):
            W[i, j] = np.exp(-((i - 1) ** 2 + (j - 1) ** 2) / 2.0)

    W /= W.sum()
    W = np.resize(W, [1, 1, 3, 3])

    # this basically used the efficient nn.Conv2d pytorch function to perform the convolution, most of the code
    # here is bureaucratic: it just reshapes the data into a form good for pytorch
    # the 4th argument of nn.Conv2d is the convolution stride, this being set to 2 is what produces the desired
    # downscaling effect, the convolution is effectively a sliding window of size 3x3 which slides over the image
    # in jumps of 2 pixels, hence the final image is half the size in each dimension
    conv = nn.Conv2d(in_channels=1, out_channels=1, kernel_size=3, stride=2, padding=0, bias=False)
    conv.weight = nn.Parameter(t.from_numpy(W).float())
    convolved_maps = conv.forward(pad_repeating(Variable(t.from_numpy(maps.reshape(-1, 1, maps.shape[1], maps.shape[2]))), 1))
    return convolved_maps.data.numpy()

# if the 1024x1024 maps are not yet in numpy format, call ringeval_fits_to_numpy,
# which makes them to numpy arrays and saves them
if 'ringeval_g_maps.npy' not in os.listdir('../data') or 'ringeval_s_maps.npy' not in os.listdir('../data'):
    import preprocessing.ringeval_fits_to_numpy

# load the maps produced by ringeval_fits_to_numpy
g_maps = np.load(save_size_1024_ringeval_g_maps_to)
s_maps = np.load(save_size_1024_ringeval_s_maps_to)
a_maps = np.load(save_size_1024_ringeval_a_maps_to)

# convolve the g_maps and s_maps and save them to file, specifying that we want them in 32-bit floating point type
np.save(save_convolved_ringeval_g_maps_to, convolve(g_maps).astype(np.float32))
np.save(save_convolved_ringeval_s_maps_to, convolve(s_maps).astype(np.float32))
# for the answer maps, we also convolve them, but the convolution produces a map which no longer contains
# only ones and zeros, so we treshold at 0.01 it to make it contain values in {0,1}. Every pixel in a_maps larger
# than 0.01 is set to 1, and everything else set to 0. the distribution of pixel values in a_maps is really bimodal,
# with most of the probability mass being close to 0 and 1, so the 0.01 is a reasonable choice which produces an
# answer map with long, connected strings, choosing higher treshold values may break those strings into smaller pieces
np.save(save_convolved_ringeval_a_maps_to, (convolve(a_maps)>thresholding_for_ringeval_a_maps).astype(np.float32))





