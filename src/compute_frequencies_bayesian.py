"""
By Razvan Ciuca, 2017

This script computes the quantities necessary to rescale the maps and compute the posteriors in bayesian.py

NEW FUNCTIONALITY:

1. Faster algorithm for frequency calculation, now using more efficient (but less intuitive) numpy routines instead
of doing everything in terms of index masks

2. All the relevant computation parameters are in config.py in the compute_frequencies section

3. Now compute_frequencies automatically saves its output inside the model object, see model_def.py for more details.
it also contains the option of saving the frequencies separatly, but this should only be maybe necessary if you want to 
visualize the frequencies directly, for the bayesian calculation the appropriate routines access the model frequencies
themselves

"""


import torch as t
from torch.autograd import Variable
import torch.nn.functional as F
import torch.nn as nn
import numpy as np
import numpy.fft as fft
import pickle
import scipy.misc

from feeder import DataFeeder
from model_def import *
from config import *
from utils import *


# method which computes the frequencies
def compute_frequencies(model, save_frequencies_to=None, save_model_to=None, string_type='cmbedge',
                        f_divisions=global_f_divisions, starting_gmu=global_starting_gmu, final_gmu=global_final_gmu,
                        gmu_divisions=global_gmu_divisions):

    P_string_given_f_Gmu = {}
    P_f_given_Gmu_string = {}

    # load the maps we use
    if string_type == 'cmbedge':
        g_maps = np.load(cmbedge_g_frequency_calculation_filename)
        s_maps = np.load(cmbedge_s_frequency_calculation_filename)
        # make sure that a_maps is full of True and False, it should already be a boolean map, but we make sure
        a_maps = np.load(cmbedge_a_frequency_calculation_filename) > 0.5
        # remove the border around a_maps to avoid edge effects from non-periodic maps
        a_maps = a_maps[:, :, border:-border, border:-border]
    else:
        g_maps = np.load(ringeval_g_frequency_calculation_filename)
        s_maps = np.load(ringeval_s_frequency_calculation_filename)
        # make sure that a_maps is full of True and False, it should already be a boolean map, but we make sure
        a_maps = np.load(ringeval_a_frequency_calculation_filename) > 0.5
        # remove the border around a_maps to avoid edge effects from non-periodic maps
        a_maps = a_maps[:, :, border:-border, border:-border]

    # send to pytorch, we use the numpy type np.uint8 so that pytorch loads it as a ByteTensor
    a_maps = t.from_numpy(a_maps.astype(np.uint8))
    a_maps = a_maps.cuda() if t.cuda.is_available() else a_maps

    # iterate over Gmu in logarithmic steps, all the parameters are in config.py
    for Gmu in [starting_gmu * (final_gmu / starting_gmu) ** (i / gmu_divisions) for i in range(0, gmu_divisions)]:

        print("doing Gmu=" + str(Gmu))

        # make a dataset of sky maps with the given Gmu
        maps = Variable(t.from_numpy(normalize_map(g_maps + Gmu * s_maps)))
        # uses the least amount of memory possible when evaluating model. When training a model, pytorch is aware
        # that we will want to differentiate through it afterwards, so it stores all sort of information to make the
        # backward pass much more efficient, however, here we will not differentiate through the model, so we don't
        # care about that stuff, we just want less memory usage
        maps.volatile = True
        # if possible use the gpu
        if t.cuda.is_available():
            maps = maps.cuda()

        # total_maps is likely too large to compute all the predictions at once, even on gpu, so we split the dataset
        # and compute predictions in chunks of n at a time
        predictions = [model.forward(maps[i:i + batch_size_for_frequency_calculation]).data[:, :, border:-border, border:-border]
                       for i in range(0, n_maps_used_for_frequency_calculation, batch_size_for_frequency_calculation)]
        # concatenate the predictions and convert to numpy
        predictions = t.cat(predictions, 0)
        print('finished computing predictions')

        # compute histogram of values for this Gmu
        # bin the predictions into f_divisions bins containing the same number of datapoints in each
        hist, intervals = constant_bin_height_histogram_pytorch(predictions, f_divisions)
        P_string_given_f_Gmu[Gmu] = [intervals, []]

        # first, use pytorch to sort the predictions, indices contains a pytorch.LongTensor containing the indices
        # of the relevant sorted values, so indices[0] is the index in prediction.view(-1) of the smallest element
        sorted_pred, indices = t.sort(predictions.view(-1))
        # use indices to rearange the answer map in the same order as the sorted prediction, now
        # sorted_answers[i] contains the answer value at the location of the i-th smallest prediction value
        sorted_answers = a_maps[0:n_maps_used_for_frequency_calculation].view(-1)[indices]
        n_per_bin = int(sorted_answers.size(0) / f_divisions)

        # for each f_divisions, go up n_per_bin into the sorted_answers and find the ratio of pixels containing a string
        for i in range(0, f_divisions):
            P_string_given_f_Gmu[Gmu][1].append(sorted_answers[i*n_per_bin: (i+1)*n_per_bin].sum()/n_per_bin)

        print('finished computing P_string_given_f_Gmu')

        # compute the probability distribution of the network output for 1. pixels which are on a string and
        # 2. pixels which are not on a string
        # predictions.masked_select(mask) is the pytorch equivalent (and gpu-compatible) version of simply doing
        # prediction[mask] in numpy, where mask is an array of trues and falses selecting which indices you want to keep
        mask = a_maps[0:n_maps_used_for_frequency_calculation]
        hist_on_string, intervals_on_string = constant_bin_height_histogram_pytorch(predictions.masked_select(mask), f_divisions)
        hist_off_string, intervals_off_string = constant_bin_height_histogram_pytorch(predictions.masked_select(1-mask), f_divisions)

        P_f_given_Gmu_string[Gmu] = [[hist_on_string, intervals_on_string], [hist_off_string, intervals_off_string]]

        print('finished computing P_f_given_Gmu_string')

    # if for some reason you want to also save the frequencies separately, here you go
    if save_frequencies_to is not None:
        pickle.dump([P_string_given_f_Gmu,
                     P_f_given_Gmu_string], open(save_frequencies_to, 'wb'))

    # attach the frequencies to the model and save it, also append to its history mentioning the details of computation
    if save_model_to is not None:
        model.add_frequencies([P_string_given_f_Gmu, P_f_given_Gmu_string], string_type, other_comments=
                              ' f_divisions : ' + str(f_divisions) + ', Gmu divisions : ' + str(gmu_divisions) +
                              ', starting Gmu : ' + str(starting_gmu) + ', final Gmu : ' + str(final_gmu))
        # need to send it to cpu first, otherwise load_model will only work on gpu for gpu-save models
        model = model.cpu()
        model.save(save_model_to)
        model = model.cuda() if t.cuda.is_available() else model

    return [P_string_given_f_Gmu, P_f_given_Gmu_string]


# main function
def frequency_calculation_main():
    # change the model_filename below to the model you want to use
    model_filename = compute_frequencies_for_this_model_filename
    model = load_model(model_filename)

    if t.cuda.is_available():
        model = model.cuda()

    frequencies = compute_frequencies(model, save_frequencies_to=also_save_frequencies_to, save_model_to=model_filename,
                                      string_type=frequency_calculation_string_type)


if __name__ == '__main__':
    frequency_calculation_main()




